import datetime
import dateutil.parser
import xbmc
import html
import urllib.parse


def colorize(text, color):
    """
    color: a hex color string (RRGGBB or #RRGGBB) or None
    """
    if not color:
        return text
    if color.startswith('#'):
        color = color[1:]
    return '[COLOR ff' + color + ']' + text + '[/COLOR]'


def format_title_and_subtitle(title, subtitle=None):
    label = u'[B]{title}[/B]'.format(title=html.unescape(title))
    # suffixes
    if subtitle:
        label += u' - {subtitle}'.format(subtitle=html.unescape(subtitle))
    return label


def encode_string(str):
    return urllib.parse.quote_plus(str, encoding='utf-8', errors='replace')


def decode_string(str):
    return urllib.parse.unquote_plus(str, encoding='utf-8', errors='replace')


def parse_date(datestr):
    date = None
    try:
        date = dateutil.parser.parse(datestr)
    except dateutil.parser.ParserError as e:
        logmsg = "[{addon_id}] Problem with parsing date: {error}".format(addon_id="plugin.video.arteplussept", error=e)
        xbmc.log(msg=logmsg, level=xbmc.LOGWARNING)
    return date


def past_week():
    today = datetime.date.today()
    one_day = datetime.timedelta(days=1)

    for i in range(0, 8):  # TODO: find better interval
        yield today - (one_day * i)

def get_image_resolution(item, resolution):
    images = item.get('images')
    if images is None:
        return None
    landscape = images.get('landscape')
    if landscape is None:
        return None
    for res in landscape.get('resolutions'):
        if res.get('w') == 720:
            return res.get('url')

def filter_zone(item):
    if item.get('authenticatedContent') is not None:
        return False
    if len(item.get('data')) == 0:
        return False
    link = item.get('link')
    if link is not None:
        if link.get('page') == "MOST_VIEWED":
            return False
    if (item.get('title').__contains__('(event teaser)') or
        item.get('title').startswith('Parcourir toute') or
        item.get('title').startswith('Votre semaine')):
        return False
    if item.get('id') != item.get('code').get('name'):
        return False

    return True
